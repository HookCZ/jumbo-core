/***************************************************
 * RJLib - JavaScript library                      *
 * Copyright (C) 2014 Roman Jámbor                 *
 * http://rjdev.net                                *
 ***************************************************
 * FileName: test.js                         ******
 * Written by: Roman Jámbor                  *****
 ***********************************************/

if (!jumbo) var jumbo = {};

/**
 * Metoda pro porovnání rovnosti dvou prvků
 * @param {Object} a
 * @param {Object} b
 * @returns {boolean}
 */
if (!jumbo.isEquals) jumbo.isEquals = function(a, b) {
	if (typeof a === "undefined" || typeof b === "undefined" || a === null || b === null) {
		return false;
	}

	if (typeof a !== typeof b) {
		return false;
	} else if (typeof a !== "object") {
		return a === b;
	}

	// Dále pokračujeme pouze pokud se jedná o objekty

	// Konstruktory se musejí shodovat
	if (a.constructor !== b.constructor) {
		return false;
	}

	// Všechny prvky z a musejí být v b a zároveň se musejí jejich hodnoty rovnat
	for (var i in a) {
		if (a.hasOwnProperty(i)) {
			if (!b.hasOwnProperty(i)) return false;
			if (!jumbo.isEquals(a[i], b[i])/*a[i] != b[i]*/) return false;
		}
	}

	// Všechny prvky z b musejí být v a a jejich hodnoty se musejí rovnat
	for (i in b) {
		if (b.hasOwnProperty(i)) {
			if (!a.hasOwnProperty(i)) return false;
			if (!jumbo.isEquals(a[i], b[i])/*a[i] != b[i]*/) return false;
		}
	}

	return true;
};


/**
 * Seznam testů, který se následně vyhodnotí
 * @type {{spravne: number, spatne: number, list: *[]}}
 * @private
 */
jumbo.tests = {
	/**
	 * @private
	 */
	spravne: 0,

	/**
	 * @private
	 */
	spatne: 0,

	/**
	 * @private
	 */
	list: [
		{ nazev: null, testy: [] }
	],

	/**
	 * @private
	 */
	nepojmenovano: "Nepojmenovaný test",

	/**
	 * Přidá chybu do seznamu
	 * @param id
	 * @param text
	 * @private
	 */
	addFail: function(id, text) {
		this.spatne++;
		jumbo.tests.list[id].testy.push({stav: false, text: text || jumbo.nepojmenovano});
	},

	/**
	 * Přidá úspěch do seznamu
	 * @param id
	 * @param text
	 * @private
	 */
	addSuccess: function(id, text) {
		this.spravne++;
		jumbo.tests.list[id].testy.push({stav: true, text: text || jumbo.nepojmenovano});
	},

	/**
	 * Slouží k zobrazení výsledků
	 */
	print: function() {
		console.log("Unit testy: " + this.spravne + " testů bylo úspěšných a " + this.spatne + " testů bylo neúspěšných.");

//		console.log(this.list);

		for (var i = 1; i < this.list.length; i++) {
			console.log("Test: " + this.list[i].nazev);

			for (var j = 0; j < this.list[i].testy.length; j++) {
				console.log("\t" + (this.list[i].testy[j].stav ? "++" : "xx") + " " + this.list[i].testy[j].text);
			}
		}
	}
};

/**
 * Statická třída pro testování
 * @param {String} nazevTestu Název testu
 * @param {Function} f funkce s testy
 * @
 */
jumbo.test = function(nazevTestu, f) {
	var selfObj = {
		testID: jumbo.tests.list.length
	};

	jumbo.tests.list[selfObj.testID] = {
		nazev:	nazevTestu,
		testy:	[]
	};

	/**
	 * Testuje hodnotu zda je true
	 * @param v Testovaná hodnota
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertTrue = function (v, text) {
		if (v === true) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje hodnotu zda je false
	 * @param v Testovaná hodnota
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertFalse = function (v, text) {
		if (v === false) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje zda jsou hodnoty shodné
	 * @param v První výraz
	 * @param h Druhý výraz
	 * @param [text] Název nebo popisek konkrétního testu
	 * @returns {boolean}
	 */
	selfObj.assertEquals = function (v, h, text) {
		if (jumbo.isEquals(v, h)) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text + "(Očekáváno: ,," + h + "\" obdrženo: ,," + v + "\")");
		return false;
	};

	/**
	 * Testuje zda je hodnota null
	 * @param v Testovaná hodnota
	 * @param text
	 * @returns {boolean}
	 */
	selfObj.assertNull = function (v, text) {
		if (typeof v !== "undefined" && v === null) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje zda hodnota není null
	 * @param v Testovaná hodnota
	 * @param text
	 * @returns {boolean}
	 */
	selfObj.assertNotNull = function (v, text) {
		if (typeof v !== "undefined" && v !== null) {
			selfObj.success(text);
			return true;
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Testuje zda fce vrací Error
	 * @param v Testovaná fce
	 * @param type
	 * @param text
	 * @returns {boolean}
	 */
	selfObj.assertException = function (v, type, text) {
		if (typeof v !== "function") {
			selfObj.fail(text + "| Error: Given parameter is not function");
			return false;
		}

		try {
			v();
		} catch (ex) {
			if (ex instanceof type) {
				selfObj.success(text);
				return true;
			}
		}

		selfObj.fail(text);
		return false;
	};

	/**
	 * Zapíše vlastní chybu
	 * @param text
	 */
	selfObj.fail = function(text) {
		jumbo.tests.addFail(selfObj.testID, text);
	};

	/**
	 * Zapíše vlastní úspěch
	 * @param text
	 */
	selfObj.success = function(text) {
		jumbo.tests.addSuccess(selfObj.testID, text);
	};

	f.call(selfObj);
};


if (module && module.exports) module.exports = jumbo;