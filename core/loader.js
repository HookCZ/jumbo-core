/**
 * Inicialializační soubor, který ověří a načte všechny potřebné prvky frameworku a vytvoří instanci aplikace
 *
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

// Zaznamenáme čas spučtění aplikace
console.time("Application load-time: ");

// Načteme potřebné moduly
var path = require("path"),
	fileSystem = require("fs");


// Ověření souborové struktury frameworku
(function() {
	var base = path.resolve(__dirname, "..");
	var err = false;
	var localErr = false;
	var a = [
		path.resolve(base, "app"),
		path.resolve(base, "app", "presenters"),
		path.resolve(base, "app", "sub-apps"),
		path.resolve(base, "app", "locale"),
		path.resolve(base, "app", "models"),
		path.resolve(base, "app", "templates"),
		path.resolve(base, "app", "tests"),

		path.resolve(base, "core"),

		path.resolve(base, "data"),
		path.resolve(base, "data", "uploads"),

		path.resolve(base, "public"),
		path.resolve(base, "public", "css"),
		path.resolve(base, "public", "js"),
		path.resolve(base, "public", "errs"),
		path.resolve(base, "public", "images"),


		path.resolve(base, "temp"),
		path.resolve(base, "temp", "cache"),
		path.resolve(base, "temp", "sessions")
	];

	a.forEach(function(p) {
		try {
			var stat = fileSystem.lstatSync(p);

			if (!stat.isDirectory()) {
				err = true;
				localErr = true;
			}
		} catch (ex) {
			err = true;
			localErr = true;
		}

		if (localErr) {
			console.error("Structure directory '" + p + "' not found.");
			localErr = false;
		}
	});

	var confPath = path.resolve(base, "app", "config.json");
	if (!fileSystem.lstatSync(confPath).isFile()) {
		err = true;
		console.error("Application config '" + confPath + "' not found.");
	}

	try {
		require(confPath);
	} catch (ex) {
		err = true;
		console.error("Config has bad format.");
	}

	if (err == true) {
		process.exit(0);
	}
})();


/**
 * Přidáme _class do globálního scope, aby sloužílo opravdu jako základní integrovaný prvek
 * @type {_class}
 * @public
 */
global._class = require("./jumbo-lib/_class.js");
global.Class = require("./jumbo-lib/class.js"); // v2


/**
 * Deklarace globálního objektu/namespace Jumbo, který představuje objekt frameworku s daty
 * @namespace
 */
global.Jumbo = {
	CONFIG_PATH: path.resolve(__dirname, "..", "app", "config.json"),
	BASE_DIR: path.resolve(__dirname, ".."),
	CORE_DIR: __dirname,
	PUBLIC_DIR: path.resolve(__dirname, "..", "public"),
	APP_DIR: path.resolve(__dirname, "..", "app"),
	ERR_DIR: path.resolve(__dirname, "..", "public", "errs"),
	UPLOAD_DIR: path.resolve(__dirname, "..", "data", "uploads"),
	CACHE_DIR: path.resolve(__dirname, "..", "temp" , "cache")
};


// Načteme autoloader
var autoloader = require("./autoloader/autoloader.js");


/**
 * Přidáme autoloader.App do globálního scope
 * @namespace
 */
global.App = autoloader.App;


// Přidáme položky z Core do namespace/objektu Jumbo
(function() {
	var objs = Object.getOwnPropertyNames(autoloader.Core);
	var c = objs.length;
	for (var p = 0; p < c; p++) {
		global.Jumbo[objs[p]] = autoloader.Core[objs[p]];
	}
})();


// Vytvoříme instanci aplikace
var app = new Jumbo.Application.Application();


/**
 * @ignore
 * @type {Jumbo.Application.Application}
 */
global.Application = app;


// Projdeme testy


// Vyexportujeme aplikaci z modulu
module.exports.application = app;
