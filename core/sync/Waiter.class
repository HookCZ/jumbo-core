/**
 * This file is part of Jumbo framework for Node.js
 * Written by Roman Jámbor
 */

/**
 * @namespace Jumbo.Sync
 */

/**
 * Třída, která po zadaném počtu nakopnutí zavolá callback
 * @class
 * @memberOf Jumbo.Sync
 */
var Waiter = _class(
	/**
	 * @lends Jumbo.Sync.Waiter.prototype
	 */
	{
		/**
		 * @param {Number} count Počet požadovaných nakopnutí
		 * @param {Function} func Callback funkce
		 * @constructs
		 */
		constructor: function (count, func) {
			this.total = count;
			this.count = 0;
			this.callback = func;

			if (count == 0) {
				func();
			}
		},

		/**
		 * Metoda pro nakopnutí, zvýší počítadlo a zkontroluje, zda bylo nakopnutí poslední
		 */
		kick: function () {
			if (++this.count == this.total) {
				this.callback();
			}
		}
	});

module.exports = Waiter;